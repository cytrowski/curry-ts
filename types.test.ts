import {
  simpleAdd,
  curriedAd,
  curryV0,
  curryV1,
  tuple,
  Params,
  Head,
  Tail,
  HasTail,
  ObjectInfer,
  FunctionInfer,
  ClassInfer,
  ArrayInfer,
  TypleInfer,
  Last,
  Length,
  Prepend,
  Drop,
  curryV3,
  curryV5
} from "mylib";

// $ExpectType number
simpleAdd(1, 2);

// $ExpectError
simpleAdd("1");

// $ExpectType (y: number) => number
curriedAd(1);

// $ExpectType number
curriedAd(1)(2);

const v1: tuple = ["a", 1, ["b", "c"]];
const f1 = (...args: tuple) => true;

f1("a", 42, []);

// $ExpectError
f1("a");

const f2 = (name: string, age: number, single: boolean) => true;

// $ExpectType [string, number, boolean]
type t1 = Params<typeof f2>;

// $ExpectType 1
type t2 = Head<[1, 2, string, number]>;

// $ExpectType string
type t3 = Head<Params<typeof f2>>;

// $ExpectType [2, string, number]
type t4 = Tail<[1, 2, string, number]>;
// $ExpectType [number, boolean]
type t5 = Tail<Params<typeof f2>>;
// $ExpectType [boolean]
type t6 = Tail<t5>;

type params = [1, 2, string];

// $ExpectType true
type t7 = HasTail<params>;
// $ExpectType true
type t8 = HasTail<Tail<params>>;
// $ExpectType false
type t9 = HasTail<Tail<Tail<params>>>;

const object = { a: "hello" };
// $ExpectType string
type t10 = ObjectInfer<typeof object>;
// $ExpectType never
type t11 = ObjectInfer<string>;

const f3 = (a: number, b: any) => true;

// $ExpectType [[number, any], boolean]
type t12 = FunctionInfer<typeof f3>;

const promise = new Promise<string>(() => {});

// $ExpectType string
type t13 = ClassInfer<typeof promise>;

const array = [0, "data", 1, "data"];
// $ExpectType string | number
type t14 = ArrayInfer<typeof array>;

// $ExpectType [string, number | boolean]
type t15 = TypleInfer<[string, number, boolean]>;

/**
 * We tried to infer the type of the rest of the tuple into a type B but it did not work as expected.
 * It is because TypeScript lacks of a feature that would allow us to deconstruct a tuple into another one.
 * There is an active proposal that tackles these issues and you can expect improved manipulation
 * for tuples in the future. This is why Tail is constructed the way it is.
 */

const toCurry1 = (name: string, age: number, single: boolean) => true;
const curried1 = (name: string) => (age: number) => (single: boolean) => true;

const toCurry2 = (name: string, age: number, single: boolean) => true;

// $ExpectType CurryV0<[string, number, boolean], boolean>
const curried2 = curryV0(toCurry2);

// $ExpectType boolean
const v2 = curried2("Jane")(26)(true);

// $ExpectType CurryV0<[string, number, boolean], boolean>
const curriedStep1 = curryV0(toCurry2);
// $ExpectType CurryV0<[number, boolean], boolean>
const curriedStep2 = curriedStep1("Jane");
// $ExpectType CurryV0<[boolean], boolean>
const curriedStep3 = curriedStep2(26);
// $ExpectType boolean
const curriedResult = curriedStep3(true);

// $ExpectError
const differentCurryCall = curriedStep1("Jane", 26)(true);
// $ExpectError
const wrongCurryCall = curriedStep1("Jane")("26")(true);

const toCurry3 = (name: string, age: number, ...nicknames: string[]) => true;
const curried3 = curryV0(toCurry3);

// This should not be an error but we will catch it anyway to go past CurryV0
// $ExpectError
const curryResult = curried3("Jane")(26)("JJ", "Jini");

const curried3_1 = curryV1(toCurry3);

// This actually should return boolean type - CurryV1 is not good either
// $ExpectType CurryV1<[number, ...string[]], boolean>
const curry3Result = curried3_1("Jane", 26, "JJ", "Jini");

// $ExpectType 4
type t16 = Last<[1, 2, 3, 4]>;

// $ExpectType 0
type t17 = Length<[]>;
// $ExpectType 2
type t18 = Length<[any, any]>;
// $ExpectType 3
type t19 = Length<[any, any, any]>;

// $ExpectType [string]
type t20 = Prepend<string, []>;
// $ExpectType [number, 1, 2]
type t21 = Prepend<number, [1, 2]>;

// $ExpectType [2, 3]
type t22 = Drop<2, [0, 1, 2, 3]>;
// $ExpectType [2]
type t23 = Drop<Length<t22>, [0, 1, 2]>;
// $ExpectType []
type t24 = Drop<4, [0, 1, 2]>;

type paramsForCurry = [string, number, boolean, string[]];
type consumedByCurry = [string, number];

// $ExpectType [boolean, string[]]
type toConsume = Drop<Length<consumedByCurry>, paramsForCurry>;

const toCurry4 = (name: string, age: number, single: boolean) => true;
const curried4_1 = curryV3(toCurry4);

// $ExpectType boolean
const r411 = curried4_1("Jane")(26)(true);
// $ExpectType boolean
const r412 = curried4_1("Jane", 26, true);
// $ExpectType boolean
const r413 = curried4_1("Jane", 26)(4000);

const toCurry5 = (name: string, age: number, ...nicknames: string[]) => true;
const curried5_1 = curryV3(toCurry5);

// IGNORE $ExpectType boolean
const r511 = curried5_1("Jane")(26)("Steve", "John");
// IGNORE $ExpectType boolean
const r512 = curried5_1("Jane", 26, "Steve", "John");
// IGNORE $ExpectType boolean
const r513 = curried5_1("Jane", 26)("Steve", "John");

// Testing curryV5

const foo = (
  name: string,
  age: number,
  single: boolean,
  ...nicknames: string[]
) => true;

const cFoo = curryV5(foo);

// $ExpectType boolean
const fin1 = cFoo("Jane")(26, true, "Steve", "John");
// $ExpectType boolean
const fin2 = cFoo("Jane", 26, true, "Steve", "John");
// $ExpectType boolean
const fin3 = cFoo("Jane", 26)(true, "Steve", "John");
// $ExpectError
const fin4 = cFoo("Jane", 26, true)("Steve", "John");
