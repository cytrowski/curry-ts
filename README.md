# CURRY TS

## Basic

- https://medium.com/@hernanrajchert/creating-typings-for-curry-using-ts-3-x-956da2780bbf

## Advanced

- https://www.freecodecamp.org/news/how-to-master-advanced-typescript-patterns-f747e99744ab/
- https://www.freecodecamp.org/news/typescript-and-its-types-f509d799947d/
- https://hackernoon.com/testing-types-an-introduction-to-dtslint-b178f9b18ac8
- https://koerbitz.me/posts/unit-testing-typescript-types-with-dtslint.html
- https://dev.to/pirixgh/higher-type-safety-for-typescript-4703
- https://mike.works/blog/post/guide-to-typescript-ambient-declarations-13d3c45
- https://basarat.gitbooks.io/typescript/docs/types/ambient/intro.html
- https://medium.com/@hernanrajchert/literal-types-to-the-test-fb989d2d0c4a
- https://medium.com/@danvdk/a-typed-pluck-exploring-typescript-2-1s-mapped-types-c15f72bf4ca8
- https://medium.com/@juliomatcom/an-elegant-and-simple-curry-f-implementation-in-javascript-cf36252cff4c

## Code

- https://github.com/pirix-gh/medium/blob/master/types-curry-ramda/src/bonus.ts
- https://github.com/mike-north/ambient-type-testing-examples/blob/master/package.json

## Tools

- https://github.com/microsoft/dtslint
- https://github.com/microsoft/dts-gen
- https://github.com/pirix-gh/ts-toolbelt
- http://npm.taobao.org/package/dts-jest
- https://marketplace.visualstudio.com/items?itemName=febean.type-script-preview
