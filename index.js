export const f = () => undefined;
export const simpleAdd = (x, y) => x + y;

export function curry(f) {
  return function currify() {
    const args = Array.prototype.slice.call(arguments);
    return args.length >= f.length ?
      f.apply(null, args) :
      currify.bind(null, ...args)
  }
}

export const curryV0 = curry
export const curryV1 = curry
export const curryV2 = curry
export const curryV3 = curry
export const curryV4 = curry
export const curryV5 = curry
export const curryV6 = curry