// TypeScript Version: 3.5

export const simpleAdd = (x: number, y: number) => x + y;
export const curriedAd = (x: number) => (y: number) => x + y;

export type tuple = ["a", number, string[]];

/**
 * The never type represents the type of values that never occur.
 * For instance, never is the return type for a function expression or an arrow
 * function expression that always throws an exception or one that never returns;
 * Variables also acquire the type never when narrowed by any type guards that can never be true.
 *
 * The never type is a subtype of, and assignable to, every type;
 * however, no type is a subtype of, or assignable to, never (except never itself).
 * Even any isn’t assignable to never.
 */

export type Params<F extends (...args: any[]) => any> = F extends ((
  ...args: infer A
) => any)
  ? A
  : never;

export type Head<T extends any[]> = T extends [any, ...any[]] ? T[0] : never;
export type Tail<T extends any[]> = ((...t: T) => any) extends ((
  _: any,
  ...tail: infer TT
) => any)
  ? TT
  : [];

export type HasTail<T extends any[]> = T extends ([] | [any]) ? false : true;

export type ObjectInfer<X> = X extends { a: infer A } ? A : never;
export type FunctionInfer<F> = F extends (...args: infer A) => infer R
  ? [A, R]
  : never;
export type ClassInfer<I> = I extends Promise<infer G> ? G : never;
export type ArrayInfer<T> = T extends Array<infer U> ? U : never;
export type TypleInfer<T> = T extends [infer A, ...(Array<infer B>)]
  ? [A, B]
  : never;

export type CurryV0<P extends any[], R> =
  // A "classic curry" takes only a single argument at a time
  (
    arg0: Head<P>
  ) => HasTail<P> extends true // If we did not reach the end of the parameters, recurse
    ? CurryV0<Tail<P>, R> // Otherwise, infer the return type of the curried function
    : R;

export function curryV0<P extends any[], R>(
  f: (...args: P) => R
): CurryV0<P, R>;

export type CurryV1<P extends any[], R> = (
  arg0: Head<P>,
  ...rest: Tail<Partial<P>>
) => HasTail<P> extends true ? CurryV1<Tail<P>, R> : R;

export function curryV1<P extends any[], R>(
  f: (...args: P) => R
): CurryV1<P, R>;

/**
 * There, we made use of a constrained generic called T that is going to track
 * any taken arguments. But now, it is completely broken, there is no more type
 * checks because we said that we wanted to track any[] kind of
 * parameters (the constraint). But not only that, Tail is completely useless
 * because it only worked well when we took one argument at a time.
 */
export type CurryV2<P extends any[], R> = <T extends any[]>(
  ...args: T
) => HasTail<P> extends true ? CurryV2<Tail<T>, R> : R;

// We won't use this one. It's broken
export function curryV2<P extends any[], R>(
  f: (...args: P) => R
): CurryV2<P, R>;

// MOOOOORE TOOLS
export type Last<T extends any[]> = {
  0: Last<Tail<T>>;
  1: Head<T>;
}[HasTail<T> extends true ? 0 : 1];

/**
 * A Type<X, Y, ...> {
 *   0 - inner type
 *   1 - inner type
 *   2 - inner type
 *   ...
 * }[Conditional Accessor]
 *
 * eg.
 * Conditional Accessor
 * X extends number
 * ? 1
 * : Y extends ...
 *   ? 0
 *   : 2
 * ( 0 | 1 | 2 | ... )
 */

export type Length<T extends any[]> = T["length"];

export type Prepend<E, T extends any[]> = ((
  head: E,
  ...args: T
) => any) extends ((...args: infer U) => any)
  ? U
  : T;

export type Drop<N extends number, T extends any[], I extends any[] = []> = {
  0: Drop<N, Tail<T>, Prepend<any, I>>;
  1: T;
}[Length<I> extends N ? 1 : 0];

// Go back to Curry...
export type CurryV3<P extends any[], R> = <T extends any[]>(
  ...args: T
) => Length<Drop<Length<T>, P>> extends 0 ? R : CurryV3<Drop<Length<T>, P>, R>;

export function curryV3<P extends any[], R>(
  f: (...args: P) => R
): CurryV3<P, R>;

// I skip CurryV4... going straight to CurryV5
export type Cast<X, Y> = X extends Y ? X : Y;

export type CurryV5<P extends any[], R> = <T extends any[]>(
  ...args: Cast<T, Partial<P>>
) => Drop<Length<T>, P> extends [any, ...any[]]
  ? CurryV5<Cast<Drop<Length<T>, P>, any[]>, R>
  : R;

export function curryV5<P extends any[], R>(
  f: (...args: P) => R
): CurryV5<P, R>;
