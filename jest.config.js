module.exports = {
  rootDir: ".",
  // preset: "ts-jest",
  testEnvironment: "node",
  modulePathIgnorePatterns: ["<rootDir>/src/types.*"],
  moduleNameMapper: {
    "^mylib$": "<rootDir>/index.js"
  }
};
